﻿namespace Fable.Import.Antd

open Fable.Core

type IReactProperty = Feliz.IReactProperty
type ReactElement = Fable.React.ReactElement

open Fable.Core.JsInterop
open Feliz

[<AutoOpen>]
module private Helpers =
    let createElement comp (props: IReactProperty seq) children =
        Interop.reactApi.createElement(comp, createObj !!props, children)
    let createNestedElement x memb props children =
        createElement (!!x?(memb)) props children
    let mkAttr = Interop.mkAttr

type IListProperty<'T> =
    inherit IReactProperty


type Antd =
    static member Layout props children = createElement (import "Layout" "antd") props children
    static member DatePicker props children = createElement (import "DatePicker" "antd") props children
    static member Button props children = createElement (import "Button" "antd") props children
    static member Menu props children = createElement (import "Menu" "antd") props children
    static member Tree props children = createElement (import "Tree" "antd") props children
    static member List (props: IListProperty<'T> seq) = createElement (import "List" "antd") (props |> Seq.cast) []
    static member Input props children = createElement (import "Input" "antd") props children
    static member Space props children = createElement (import "Space" "antd") props children


[<AutoOpen>]
module Antd =

    type Layout =
        static member Header props children = createNestedElement (import "Layout" "antd") "Header" props children
        static member Sider props children = createNestedElement (import "Layout" "antd") "Sider" props children
        static member Content props children = createNestedElement (import "Layout" "antd") "Content" props children

        static member className (x: string) = mkAttr "className" x
        static member hasSider (x: bool) = mkAttr "hasSider" x
        static member style (x: IStyleAttribute seq) = mkAttr "style" (Seq.toArray x)

    module Layout =
        type Header =
            static member className (x: string) = mkAttr "className" x
            static member hasSider (x: bool) = mkAttr "hasSider" x
            static member style (x: IStyleAttribute seq) = mkAttr "style" (Seq.toArray x)
        type Footer =
            static member className (x: string) = mkAttr "className" x
            static member hasSider (x: bool) = mkAttr "hasSider" x
            static member style (x: IStyleAttribute seq) = mkAttr "style" (Seq.toArray x)
        type Content =
            static member className (x: string) = mkAttr "className" x
            static member hasSider (x: bool) = mkAttr "hasSider" x
            static member style (x: IStyleAttribute seq) = mkAttr "style" (Seq.toArray x)
        type Sider =
            static member breakpoint (x: Breakpoint) = mkAttr "breakpoint" x
            static member collapsed (x: bool) = mkAttr "collapsed" x
            static member theme (x: Theme) = mkAttr "theme" x
            static member width (x: int) = mkAttr "width" x
            static member width (x: string) = mkAttr "width" x

    type Menu =
        static member Item props children = createNestedElement (import "Menu" "antd") "Item" props children

        static member mode (x: MenuMode) = mkAttr "mode" x
        static member theme (x: Theme) = mkAttr "theme" x

    module Menu =
        type Item =
            static member disabled (x: bool) = mkAttr "disabled" x
            static member key (x: string) = mkAttr "key" x
            static member title (x: string) = mkAttr "title" x
            static member icon (x: ReactElement) = mkAttr "icon" x
            static member danger (x: bool) = mkAttr "danger" x

    type Tree =
        static member treeData (x: TreeData seq) =
            let rec formatTreeData (x: TreeData seq) : obj array =
                x |> Seq.map (fun x ->
                    createObj [
                        "key" ==> x.Key
                        "title" ==> x.Title
                        "children" ==> formatTreeData x.Children
                    ]
                ) |> Seq.toArray
            mkAttr "treeData" (formatTreeData x)

    type List =
        static member Item props children = createNestedElement (import "List" "antd") "Item" props children
        static member dataSource (x: 'T seq) = mkAttr "dataSource" (Seq.toArray x) :?> IListProperty<'T>
        static member renderItem (x: 'T -> ReactElement) = mkAttr "renderItem" x :?> IListProperty<'T>

    module List =
        type Item =
            class end

    type Input =
        static member placeholder (x: string) = mkAttr "placeholder" x


    type Space =
        static member align (x: SpaceAlign) = mkAttr "align" x
        static member direction (x: SpaceDirection) = mkAttr "direction" x
        static member size (x: SpaceSize) = mkAttr "size" x

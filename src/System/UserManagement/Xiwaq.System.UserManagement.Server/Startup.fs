[<RequireQualifiedAccess>]
module Xiwaq.System.UserManagement.Server.Startup

open System
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Hosting
open Microsoft.Extensions.Configuration
open Microsoft.Extensions.DependencyInjection
open Microsoft.Extensions.Logging
open Giraffe
open Giraffe.Serialization.Json
open Thoth.Json.Giraffe

open Xiwaq.Framework.UserManagement
open Xiwaq.System.UserManagement.Server.App
open Xiwaq.System.UserManagement.Server.Authentication
open Xiwaq.System.UserManagement.Server.Library
open Xiwaq.System.UserManagement.Server.Infrastructure.Extensions


let errorHandler (ex: exn) (logger: ILogger) =
    logger.LogError(ex, "An unhandled exception has occurred while executing the request.")
    clearResponse >=> setStatusCode 500 >=> text ex.Message


let configureServices (config: IConfiguration) (services: IServiceCollection) : unit =

    let connectionString = config.GetConnectionString("Default")
    services.AddDataConnection(connectionString) |> ignore

    services.AddScoped<_>(Func<_, _> AppReader.factory) |> ignore
    services.Configure<AuthenticationOptions>(config.GetSection("AuthOptions")) |> ignore
    services.AddTransient<IAuthenticationService, AuthenticationService>() |> ignore
//    services.AddSingleton<IJsonSerializer, ThothSerializer>() |> ignore
    services.AddGiraffe() |> ignore


let configureApp (config: IConfiguration) (app: IApplicationBuilder) : unit =
    let env = app.ApplicationServices.GetService<IWebHostEnvironment>()
    (match env.EnvironmentName with
    | "Development" -> app.UseDeveloperExceptionPage()
    | _ -> app.UseGiraffeErrorHandler(errorHandler)
    )
        .UseGiraffe(HttpHandlers.webapp)



let configureLogging (builder: ILoggingBuilder) : unit =
    builder.AddConsole() |> ignore

module Xiwaq.System.UserManagement.Server.Infrastructure.DaoMappers

open Xiwaq.Framework.UserManagement
open Xiwaq.Framework.UserManagement.Wrappers
open Xiwaq.Framework.UserManagement.Unwrappers
open Xiwaq.System.UserManagement.Server.DomainTypes
open Xiwaq.System.UserManagement.Server.Infrastructure.DaoTypes


module UserDomain =

    let ofDao (userDao: UserDao) : UserDomain =
        let user =
            { Id = UserId.Unchecked.create userDao.Id
              FullName = UserFullName.Unchecked.create (UserName.Unchecked.create userDao.Name, UserTag.Unchecked.create (uint16 userDao.Tag)) }
        { User = user
          Email = UserEmail.Unchecked.create userDao.Email
          PasswordHash = UserPasswordHash userDao.PasswordHash }

    let toDao { User = { Id = UserId id
                         FullName = UserFullName (UserName name, UserTag tag) }
                Email = UserEmail email
                PasswordHash = UserPasswordHash passwordHash } =
        { Id = id; Name = name; Tag = int16 tag; Email = email; PasswordHash = passwordHash }

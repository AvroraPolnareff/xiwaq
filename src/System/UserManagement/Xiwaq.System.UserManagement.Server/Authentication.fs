module Xiwaq.System.UserManagement.Server.Authentication

open System
open System.IdentityModel.Tokens.Jwt
open System.Security.Claims
open System.Text
open System.Threading.Tasks

open Microsoft.AspNetCore.Authentication.JwtBearer
open Microsoft.Extensions.Options
open Microsoft.IdentityModel.JsonWebTokens
open Microsoft.IdentityModel.Tokens

open FSharp.Control.Tasks.V2

open Reader

open Xiwaq.Framework.UserManagement
open Xiwaq.Framework.UserManagement.Wrappers
open Xiwaq.Framework.UserManagement.Unwrappers


[<CLIMutable>]
type AuthenticationOptions =
    { Secret: string
      Issuer: string
      Audience: string
      Expiration: TimeSpan }

let configureJwt (authOptions: AuthenticationOptions) (options: JwtBearerOptions) =
    options.TokenValidationParameters <- TokenValidationParameters(
        ValidateActor = true,
        ValidateAudience = true,
        ValidateLifetime = true,
        ValidateIssuerSigningKey = true,
        ValidIssuer = authOptions.Issuer,
        ValidAudience = authOptions.Audience,
        IssuerSigningKey = SymmetricSecurityKey(Encoding.UTF8.GetBytes(authOptions.Secret))
        // NameClaimType = ClaimTypes.NameIdentifier
    )
    options.SaveToken <- true
    options.Events <- JwtBearerEvents(
        OnMessageReceived = fun context ->
            task {
                let accessToken = context.Request.Query.["access_token"] |> string
                if not (String.IsNullOrEmpty(accessToken)) then
                    context.Token <- accessToken
            } :> Task
    )


module Token =

    let generate (options: AuthenticationOptions) (now: DateTime) (UserId userId) =
        let claims =
            [ // Claim(ClaimTypes.Name, string userId)
              Claim(JwtRegisteredClaimNames.Sub, string userId)
              Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()) ]
        let expires = now.Add(options.Expiration)
        let notBefore = now
        let securityKey = options.Secret |> Encoding.UTF8.GetBytes |> SymmetricSecurityKey
        let signingCredentials = SigningCredentials(key = securityKey, algorithm = SecurityAlgorithms.HmacSha256)
        let token =
            JwtSecurityToken(
                issuer = options.Issuer,
                audience = options.Audience,
                claims = claims,
                expires = Nullable expires,
                notBefore = Nullable notBefore,
                signingCredentials = signingCredentials
            )
        JwtSecurityTokenHandler().WriteToken(token) |> Token.Unchecked.create

    let getClaims (secret: string) (Token token) : Result<ClaimsPrincipal, unit> =
        let validations =
            TokenValidationParameters(
                IssuerSigningKey = SymmetricSecurityKey(Encoding.UTF8.GetBytes secret)
            )
        let claims, token' = JwtSecurityTokenHandler().ValidateToken(token, validations)
        Ok claims

    open Xiwaq.Framework.Utils.HttpReader

    let getSecret: Reader<_, string> = reader {
        let! authOptions = HttpReader.resolve<IOptions<AuthenticationOptions>> |> Reader.map (fun x -> x.Value)
        return authOptions.Secret
    }

    let getClaims' token = reader {
        let! secret = getSecret
        return getClaims secret token
    }

namespace Xiwaq.System.UserManagement.Server.Domain

open BCrypt.Net
open Xiwaq.System.UserManagement.Server.DomainTypes


module UserPassword =

    open Xiwaq.Framework.UserManagement.Unwrappers

    let hash pwd =
        let (UserPassword pwds) = pwd
        let hashs = BCrypt.HashPassword(pwds)
        UserPasswordHash hashs

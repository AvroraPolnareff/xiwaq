namespace Xiwaq.System.WepApp.Authentication

open Xiwaq.Framework.UserManagement
open Xiwaq.Framework.UserManagement.Wrappers
open Xiwaq.Framework.UserManagement.Unwrappers


module Mock =

    open System
    open System.Text.RegularExpressions

    module private Db =

        type DbUser =
            { Id: UserId
              FullName: UserFullName
              Email: UserEmail
              Password: UserPassword }

        let mutable private users = []

        let addUser user = async { users <- user :: users }

        let tryFindUserById id = async { return Seq.tryFind (fun u -> u.Id = id) users }

        let tryFindUserByEmailAndPassword email password = async {
            return Seq.tryFind (fun u -> u.Email = email && u.Password = password) users
        }


    let genIdByEmail (email: UserEmail) =
        let s = email |> fun (UserEmail x) -> x
        let seed = s.GetHashCode()
        let bytes = Array.zeroCreate 16
        let rand = Random(seed)
        rand.NextBytes(bytes)
        Guid(bytes) |> UserId.Unchecked.create

    let encodeToken (id: UserId) =
        InputToken ^ sprintf "xxxTOKEN__%s__TOKENxxx" (id |> fun (UserId x) -> x |> string)

    let decodeToken (Token token) =
        let m = Regex.Match(token, @"xxxTOKEN__(.+?)__TOKENxxx")
        if m.Success
        then
            let id = m.Groups.[1].Value |> Guid.Parse |> UserId.Unchecked.create
            Ok id
        else
            Error ()


    let ServerLatency = 1000


    let getMe inputToken = asyncResult {
        let token = inputToken |> fun (InputToken x) -> Token.Unchecked.create x
        do! Async.Sleep(ServerLatency)
        let! userId = decodeToken token |> Result.mapError (cnst ^ GetMeError.Other "Invalid token")
        let user = Db.tryFindUserById userId
        match! user with
        | None -> return! Error ^ GetMeError.Other "User not found"
        | Some user ->
            let me =
                { Me.Token = token
                  Me.User =
                      { User.Id = user.Id
                        User.FullName = user.FullName } }
            return me
    }

    let signIn (email: UserEmail) (password: UserPassword) = async {
        do! Async.Sleep(ServerLatency)
        let! user = Db.tryFindUserByEmailAndPassword email password
        return!
            match user with
            | None -> Error ^ SignInError.Other "Failed to Sign In [Mock]" |> Async.retn
            | Some user ->
                let token = encodeToken user.Id
                let me = getMe token
                me |> Async.map (Result.getOk >> Ok)
    }

    // If password is "fail" return Error
    let signUp email name password : AsyncResult<unit, SignUpError> = asyncResult {
        do! Async.Sleep(ServerLatency)
        if (password |> fun (UserPassword x) -> x) = "fail" then
            return! Error ^ SignUpError.Other "Password 'fail' isn't allowed [Mock]"
        else
        let userId = Guid.NewGuid() |> UserId.Unchecked.create
        let userTag = Random().Next() |> uint16 |> UserTag.Unchecked.create
        do! Db.addUser
                { Db.Id = userId
                  Db.Email = email
                  Db.FullName = UserFullName.Unchecked.create (name, userTag)
                  Db.Password = password }
    }

    type MockAuthenticationService() =
        interface IAuthenticationService with
            member _.SignUp(rq) = signUp rq.Email rq.Name rq.Password
            member _.SignIn(rq) = signIn rq.Email rq.Password
            member _.GetMe(token) = getMe token


open Xiwaq.Framework.UserManagement.Client.Fable


[<RequireQualifiedAccess>]
module Auth =

    let private service: IAuthenticationService =
        #if NO_SERVER
            upcast Mock.MockAuthenticationService()
        #else
            upcast FetchAuthenticationService(Unchecked.defaultof<_>)
        #endif

    let getMe token = service.GetMe(token)
    let signIn email password = service.SignIn({ Email = email; Password = password })
    let signUp name email password = service.SignUp({ Name = name; Email = email; Password = password })


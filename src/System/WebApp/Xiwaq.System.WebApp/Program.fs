﻿module Xiwaq.System.WebApp.Program

open Reader
open Xiwaq.Framework.UserManagement.Types
open Xiwaq.System.WebApp.Library
open Xiwaq.System.WebApp.Services

// Remove loading-label
Browser.Dom.document.querySelector("#loading-label").remove()


open Elmish
open Elmish.React


module Impl =
    open System
    open Xiwaq.Framework.PluginManagement
    open Xiwaq.Framework.UserManagement
    open Xiwaq.Framework.UserManagement.Client.Fable
    open Xiwaq.Framework.UserManagement.Wrappers
    open Xiwaq.Framework.PluginManagement.Client

    let tokenProvider =
        { new ITokenProvider with member _.Token = Token.Unchecked.create "" }

    let pluginMsgHost = Uri "http://localhost:5003"

    let pluginManager: IPluginManager = upcast FetchPluginManager(tokenProvider, pluginMsgHost)

    let pluginScriptLinkProvider: IPluginScriptLinkProvider = upcast PluginScriptLinkProvider(pluginMsgHost)

    let pluginLoader: IPluginLoader = upcast PluginLoader(pluginManager, pluginScriptLinkProvider)

    let userMngHost = Uri Unchecked.defaultof<_>

    let authenticationService = FetchAuthenticationService(userMngHost)

let appenv =
    { PluginLoader' = Impl.pluginLoader
      AuthenticationService' = Impl.authenticationService }


let init () = App.State.init () |> Reader.run <| appenv
let update = App.State.update
let view = App.View.view





Program.mkProgram init update view
|> Program.withReactBatched "app"
|> Program.runWith ()

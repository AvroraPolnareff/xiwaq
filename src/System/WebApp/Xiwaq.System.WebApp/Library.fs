module Xiwaq.System.WebApp.Library

open Xiwaq.Framework.PluginManagement
open Xiwaq.Framework.PluginManagement.Client


module PluginLoader =

    open Browser
    open Fable.Core
    open Browser.Types

    let private PluginCodeContainerNode: Node = upcast document.getElementById("xiwaq-plugin-code-container")

    let load (pluginScriptLinkProvider: IPluginScriptLinkProvider) (pluginManager: IPluginManager) pluginId = async {
        let! pluginInfo = pluginManager.GetInfoById(pluginId)

        let! pluginProgram =
            let loader () =
                Promise.create ^fun res _ ->
                    let scriptElement: HTMLScriptElement = downcast document.createElement("script")
                    scriptElement.src <- pluginScriptLinkProvider.GetLink(pluginId)
                    scriptElement.onload <- fun ev -> res ()
                    PluginCodeContainerNode.appendChild(scriptElement) |> ignore
                |> Async.AwaitPromise
            Internal.GlobalPluginProgramLoadedCallback.usingAsync loader

        let plugin = { Info = pluginInfo
                       Program = pluginProgram }
        return plugin
    }


type PluginLoader(pluginManager, pluginScriptLinkProvider) =
    interface IPluginLoader with
        member this.Load(pluginId) = PluginLoader.load pluginScriptLinkProvider pluginManager pluginId


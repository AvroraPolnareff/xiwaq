module Xiwaq.Framework.Elmish

open Elmish


type Init<'arg, 'msg, 'state> = 'arg -> 'state * Cmd<'msg>
type Update<'msg, 'state> = 'msg -> 'state -> 'state * Cmd<'msg>
type View<'state, 'msg, 'view> = 'state -> Dispatch<'msg> -> 'view


module Update =

    let inner (updateB: Update<'MsgB, 'StateB>) (msgB: 'MsgB) (stateA: 'StateA) (get, set) (msgF: 'MsgB -> 'MsgA) =
        let stateB, cmdB = updateB msgB (get stateA)
        set stateA stateB, Cmd.map msgF cmdB

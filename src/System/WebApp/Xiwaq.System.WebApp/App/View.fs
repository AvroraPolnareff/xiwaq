module Xiwaq.System.WebApp.App.View

open Feliz
open Elmish

open Xiwaq.Framework.PluginManagement
open Xiwaq.System.WebApp.App.State


let renderPlugin (plugin: Plugin) pstate dispatch =
    let pdispatch = (fun pmsg -> Msg.PluginCommandDispatched (plugin.Info, pmsg)) >> dispatch
    plugin.Program.view pstate pdispatch


let view (state: State) (dispatch: Dispatch<Msg>) =
    let pelements = state.Plugins |> Seq.map (fun (KeyValue (_, (plugin, dState))) -> renderPlugin plugin dState dispatch)
    React.fragment pelements


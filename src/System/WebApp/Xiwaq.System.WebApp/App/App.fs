module Xiwaq.System.WebApp.App.State

open Reader
open System
open Elmish

open FSharp.Control
open Xiwaq.Framework.UserManagement
open Xiwaq.Framework.PluginManagement
open Xiwaq.System.WebApp
open Xiwaq.System.WebApp.Services


type ControllerMsg =
    | FeedCommand of PluginDynamicCommand
    | ListenEvents of thisPluginInfo: PluginInfo * pName: string * AsyncReplyChannel<IAsyncObservable<DynamicEvent>>
    | PluginsLoaded of Map<PluginId, Plugin * DynamicState>
    | UpdateState of PluginDynamicEvent
    | GetStates of AsyncReplyChannel<IAsyncObservable<PluginDynamicState>>


type ControllerState =
    { Plugins: Map<PluginId, Plugin * DynamicState>
      // DynamicCommands: IAsyncObservable<PluginDynamicCommand>
      }

let StreamCompletedWarn = async { do Browser.Dom.console.warn("Unreachable stream completion!") }


type Controller() as this =

    let agent = MailboxProcessor<ControllerMsg>.Start(fun inbox ->
        let dCommandObv, dCommands = AsyncRx.subject<PluginDynamicCommand> ()
        let dStateObv, dStates = AsyncRx.singleSubject<PluginDynamicState> ()

        let rec loop (cState: ControllerState) = async {
            printfn "@Controller#loop "
            let! msg = inbox.Receive()
            printfn "@Controller#loop: %A" msg
            match msg with

            | GetStates channel ->
                do channel.Reply(dStates)
                return! loop cState

            | ControllerMsg.PluginsLoaded plugins ->
                return! loop { cState with Plugins = plugins }

            | FeedCommand (info, dCommand) ->
                printfn "@FeedCommand: %A | %A" (info.Name |> fun (PluginName x) -> x) dCommand
                let plugin, dState = cState.Plugins.[info.Id]
                let dEvents =
                    dCommands
                    |> AsyncRx.choose ^fun (info', dCommand) -> if info'.Id = info.Id then Some dCommand else None
                    |> plugin.Program.stream dState
                    |> fun r -> Reader.run r { Controller = this; PluginInfo = info }
                let! dEventDisp = dEvents.SubscribeAsync(function
                    | OnNext dEvent ->
                        async {
                            do inbox.Post(UpdateState ((info, dEvent)))
                        }
                    | OnCompleted -> StreamCompletedWarn
                    | OnError err -> raise err
                )
                do! dCommandObv.OnNextAsync(info, dCommand)
                do! dEventDisp.DisposeAsync()

                return! loop cState

            | UpdateState (info, dEvent) ->
                printfn "@UpdateState: %A | %A" (info.Name |> fun (PluginName x) -> x) dEvent
                let plugin, dState = cState.Plugins.[info.Id]
                let dState = plugin.Program.update dEvent dState
                do! dStateObv.OnNextAsync((info, dState))
                return! loop { cState with Plugins = cState.Plugins |> Map.add info.Id (plugin, dState) }

            | ListenEvents (thisPluginInfo, pName, channel) ->
                printfn "@UpdateState: %s listens %s" (thisPluginInfo.Name |> fun (PluginName x) -> x) pName
                let plugin, dState =
                    cState.Plugins |> Seq.pick ^fun (KeyValue (_, (plugin, dState))) ->
                        if (plugin.Info.Name |> fun (PluginName x) -> x) = pName
                        then Some (plugin, dState)
                        else None
                let dEvents =
                    dCommands
                    |> AsyncRx.choose ^fun (info', dCommand) -> if info'.Id = plugin.Info.Id then Some dCommand else None
                    |> plugin.Program.stream dState
                    |> fun r -> Reader.run r { Controller = this; PluginInfo = plugin.Info }
                let! disp = dEvents.SubscribeAsync(function
                    | OnNext dEvent ->
                        async {
                            inbox.Post(UpdateState (thisPluginInfo, dEvent))
                        }
                    | OnCompleted -> StreamCompletedWarn
                    | OnError err -> raise err
                )
                channel.Reply(dEvents)
                do! disp.DisposeAsync()
                return! loop cState
        }
        async {
            let plugins = Map.empty
            let cState =
                { Plugins = plugins }
            return! loop cState
        }
    )
//
    member this.FeedCommand(pdCommand) =
        agent.Post(FeedCommand pdCommand)

    member this.LoadPlugins(plugins) =
        agent.Post(ControllerMsg.PluginsLoaded plugins)

    member _.GetStates() = async {
        printfn "@GetStates v"
        let! x = agent.PostAndAsyncReply(GetStates)
        printfn "@GetStates ^"
        return x
    }

    interface IController with
        member this.Listen<'PEvent>(thisPluginInfo, pName) =
            AsyncRx.create ^fun (pEventObv: IAsyncObserver<'PEvent>) -> async {
                let! dEvents = agent.PostAndAsyncReply(fun ch -> ListenEvents (thisPluginInfo, pName, ch))
                let events =
                    dEvents
                    |> AsyncRx.map ^fun (DynamicEvent dEvent) -> dEvent :?> 'PEvent
                let! disp = events.SubscribeAsync(pEventObv)
                return disp
            }


module Tmp =

    let loadPlugin (pluginLoader: IPluginLoader) guid =
        async {
            let pluginId = PluginId ^ Guid(guid: string)
            let! plugin = pluginLoader.Load(pluginId)
            return plugin
        }

    let loadAllPlugins () = reader {
        let! pluginLoader = PluginLoader.get
        return async {
            let! p1 = loadPlugin pluginLoader "485fbe49-f26f-4236-8808-16d463268e10"
            let! p2 = loadPlugin pluginLoader "f301a6a7-be04-4140-bc21-4c9970998b68"
            return [ p1; p2 ] |> Seq.ofList
        }
    }



type State =
    { Authenticator: Authenticator.State
      Plugins: Map<PluginId, Plugin * DynamicState> }

type Msg =
    | AuthenticatorMsg of Authenticator.Msg

    | PluginsLoaded of Plugin seq

    | PluginCommandDispatched of PluginDynamicCommand
    | PluginStateUpdated of PluginDynamicState


let controller = Controller()


let init () = reader {
    let! loadPluginCmd = Tmp.loadAllPlugins ()
    let auth, authCmd = Authenticator.init ()
    let getStatesSub dispatch =
        async {
            printfn "@sub 1"
            let! dStates = controller.GetStates()
            printfn "@sub 2 | "// dStates
            let! disp = dStates.SubscribeAsync(function
                | OnNext (info, dState) ->
                    async {
                        printfn "@sub 4"
                        dispatch ^ PluginStateUpdated (info, dState)
                        printfn "@sub 5"
                    }
                | OnCompleted -> Async.zero
                | OnError err -> raise err
            )
            printfn "@sub 3"
        }
        |> Async.StartImmediate
    return
        { Authenticator = auth
          Plugins = Map.empty }
        , Cmd.batch [
            Cmd.map AuthenticatorMsg authCmd
            Cmd.OfAsync.perform (fun () -> loadPluginCmd) () Msg.PluginsLoaded
            Cmd.ofSub getStatesSub
        ]
}


let update (msg: Msg) (state: State) : State * Cmd<Msg> =
    match msg with
    | AuthenticatorMsg msg ->
        let auth, authCmd = Authenticator.update msg state.Authenticator
        { state with Authenticator = auth }
        , Cmd.batch [ Cmd.map AuthenticatorMsg authCmd ]

    | Msg.PluginsLoaded plugins ->
        let plugins =
            plugins
            |> Seq.map ^fun plugin ->
                let id = plugin.Info.Id
                let dState = plugin.Program.init plugin.Info
                id, (plugin, dState)
            |> Map.ofSeq
        let sub dispatch = do controller.LoadPlugins(plugins)
        { state with Plugins = plugins }
        , Cmd.ofSub sub

    | PluginCommandDispatched pdCommand ->
        printfn "@PluginCommandDispatched"
        let sub dispatch =
            printfn "@PluginCommandDispatched#sub"
            do controller.FeedCommand(pdCommand)
        state, Cmd.ofSub sub

    | PluginStateUpdated (info, dState) ->
        let plugin, _ = state.Plugins.[info.Id]
        { state with Plugins = state.Plugins |> Map.add info.Id (plugin, dState) }
        , Cmd.none


module Xiwaq.System.WebApp.Services

open Reader
open Xiwaq.Framework.PluginManagement
open Xiwaq.Framework.UserManagement

type IHasAuthenticationService = abstract AuthenticationService: IAuthenticationService
[<RequireQualifiedAccess>]
module Auth =
    let get<'env when 'env :> IHasAuthenticationService> = Reader.get<'env> |> Reader.map ^fun env -> env.AuthenticationService
    let getMe token = get |> Reader.map ^fun env -> env.GetMe(token)
    let signIn email password = get |> Reader.map ^fun env -> env.SignIn({ Email = email; Password = password })
    let signUp name email password = get |> Reader.map ^fun env -> env.SignUp({ Name = name; Email = email; Password = password })


module TokenProvider =

    let create f = { new ITokenProvider with member _.Token = f () }


type IHasPluginLoader = abstract PluginLoader: IPluginLoader
[<RequireQualifiedAccess>]
module PluginLoader =
    let get<'env when 'env :> IHasPluginLoader> = Reader.get<'env> |> Reader.map ^fun env -> env.PluginLoader
    let load pluginId = get |> Reader.map ^fun env -> env.Load pluginId


type AppEnv =
    { PluginLoader': IPluginLoader
      AuthenticationService': IAuthenticationService }
    interface IHasAuthenticationService with member this.AuthenticationService = this.AuthenticationService'
    interface IHasPluginLoader with member this.PluginLoader = this.PluginLoader'

module Xiwaq.System.PluginManagement.Server.Startup

open Microsoft.AspNetCore.Hosting
open Microsoft.Extensions.Configuration
open Microsoft.Extensions.DependencyInjection
open Microsoft.Extensions.Logging
open Microsoft.AspNetCore.Builder
open Giraffe
open LinqToDB.AspNet
open LinqToDB.AspNet.Logging
open Xiwaq.Framework.PluginManagement
open Xiwaq.System.PluginManagement.Server.Infrastructure
open Xiwaq.System.PluginManagement.Server.Library


let configureServices (config: IConfiguration) (services: IServiceCollection) : unit =
    let connectionString = config.GetConnectionString("Default")
    services.AddLinqToDbContext<AppDataConnection>(fun provider options ->
        options
            .UsePostgreSQL(connectionString)
            .UseDefaultLogging(provider)
        |> ignore
    ) |> ignore

    services.AddTransient<IPluginManager, PluginManager>() |> ignore
    services.AddTransient<IPluginScriptProvider, HardPluginScriptProvider>() |> ignore

    services.AddTransient<AppEnv>(fun sp -> AppEnv.factory sp) |> ignore

    services.AddCors(fun options ->
        options.AddDefaultPolicy(fun builder ->
            builder
                .AllowAnyOrigin()
                .AllowAnyHeader()
                .AllowAnyMethod()
            |> ignore)
    ) |> ignore

    services.AddGiraffe() |> ignore
    ()


let errorHandler (ex: exn) (logger: ILogger) =
    logger.LogError(ex, "An unhandled exception has occurred while executing the request.")
    clearResponse >=> setStatusCode 500 >=> text ex.Message

let configureApp (config: IConfiguration) (app: IApplicationBuilder) : unit =
    let env = app.ApplicationServices.GetService<IWebHostEnvironment>()
    (match env.EnvironmentName with
    | "Development" -> app.UseDeveloperExceptionPage()
    | _ -> app.UseGiraffeErrorHandler(errorHandler)
    )
        .UseCors()
        .UseGiraffe(HttpHandlers.webapp)


let configureLogger (builder: ILoggingBuilder) =
    builder.AddConsole() |> ignore

module Xiwaq.System.PluginManagement.Server.Library

open System.IO
open Reader
open Xiwaq.Framework.PluginManagement
open Xiwaq.System.PluginManagement.Server.Domain
open Xiwaq.System.PluginManagement.Server.Infrastructure


type IPluginScriptProvider =
    abstract GetScript: PluginId -> Async<PluginScript>


type PluginScriptProvider(appenv: AppEnv) =

    interface IPluginScriptProvider with
        member _.GetScript(pluginId) =
            Repository.Queries.getScriptByPluginId pluginId |> Reader.run <| appenv

type HardPluginScriptProvider() =

    let path = "../../../Base/deploy"

    interface IPluginScriptProvider with
        member _.GetScript(pluginId) = async {
            let (PluginId pid) = pluginId
            let! script = File.ReadAllTextAsync(sprintf "%s/%s.js" path (string pid)) |> Async.AwaitTask
            return PluginScript script
        }


module PluginManager =

    let getInfoById (pluginId: PluginId) = reader {
        return! Repository.Queries.getInfoById pluginId
    }


type PluginManager(appenv: AppEnv) =
    interface IPluginManager with
        member this.GetAllPluginInfos() = notImplemented ""
        member this.GetInfoById(pluginId) = PluginManager.getInfoById pluginId |> Reader.run <| appenv

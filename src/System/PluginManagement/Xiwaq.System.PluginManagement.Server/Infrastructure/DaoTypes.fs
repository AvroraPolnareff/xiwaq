[<AutoOpen>]
module Xiwaq.System.PluginManagement.Server.Infrastructure.DaoTypes

open System


type PluginInfoDao =
    { Id: Guid
      Name: string
      AuthorId: Guid }

type PluginScriptDao =
    { PluginId: Guid
      Script: string }

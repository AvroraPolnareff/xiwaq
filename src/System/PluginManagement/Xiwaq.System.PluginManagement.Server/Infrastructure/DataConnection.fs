namespace Xiwaq.System.PluginManagement.Server.Infrastructure

open LinqToDB
open LinqToDB.Configuration
open LinqToDB.Data
open LinqToDB.Mapping


type AppDataConnection(options: LinqToDbConnectionOptions<AppDataConnection>) as this =

    inherit DataConnection(options)

    let mapping (schema: MappingSchema) =
        let mb = schema.GetFluentMappingBuilder()
        mb.Entity<PluginInfoDao>()
            .HasTableName("PluginInfos")
            .HasPrimaryKey(fun x -> x.Id)
        |> ignore
        mb.Entity<PluginScriptDao>()
            .HasTableName("PluginScripts")
        |> ignore

    do mapping this.MappingSchema

    member this.PluginInfos = this.GetTable<PluginInfoDao>()
    member this.PluginScripts = this.GetTable<PluginScriptDao>()

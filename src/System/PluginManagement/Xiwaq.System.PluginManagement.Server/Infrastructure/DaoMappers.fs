module Xiwaq.System.PluginManagement.Server.Infrastructure.DaoMappers

open Xiwaq.Framework.UserManagement.Wrappers
open Xiwaq.Framework.PluginManagement
open Xiwaq.Framework.PluginManagement.Wrappers


module PluginInfo =
    let ofDao (dao: PluginInfoDao) : PluginInfo =
        { Id = dao.Id |> PluginId.Unchecked.create
          Name = dao.Name |> PluginName.Unchecked.create
          AuthorId = dao.AuthorId |> UserId.Unchecked.create }

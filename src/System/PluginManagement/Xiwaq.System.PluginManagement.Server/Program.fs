﻿module Xiwaq.System.PluginManagement.Server.Program

open Microsoft.AspNetCore
open Microsoft.AspNetCore.Hosting
open Microsoft.Extensions.Configuration


type Startup(configuration: IConfiguration) =
    member _.ConfigureServices(services) = Startup.configureServices configuration services
    member _.Configure(app) = Startup.configureApp configuration app

[<CompiledName "CreateHostBuilder">]
let createHostBuilder(args: string[]) : IWebHostBuilder =
    WebHost.CreateDefaultBuilder(args)
        .ConfigureLogging(Startup.configureLogger)
        .UseStartup<Startup>()


[<EntryPoint; CompiledName "Main">]
let main args =
    createHostBuilder(args).Build().Run()
    0

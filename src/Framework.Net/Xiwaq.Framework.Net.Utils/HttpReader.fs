module Xiwaq.Framework.Utils.HttpReader

open Microsoft.AspNetCore.Http
open Microsoft.Extensions.DependencyInjection

open Reader


type HttpReader<'a> = Reader<HttpContext, 'a>

[<RequireQualifiedAccess>]
module HttpReader =
    let resolve<'T> = Reader.get<HttpContext> <!> (fun ctx -> ctx.RequestServices.GetRequiredService<'T>())



[<AutoOpen>]
module Builder =
    type HttpReaderBuilder() =
        member _.Bind(x, f): HttpReader<_> = Reader.bind f x
        member _.Return(x): HttpReader<_> = Reader.retn x
//        member _.Using(x, f): HttpReader<_> = use x = x in f x
        member _.Using(x, f): HttpReader<_> =
            reader {
                let! ctx = Reader.get
                ctx.Response.RegisterForDispose(x)
                return! f x
            }

    let httpReader = HttpReaderBuilder()

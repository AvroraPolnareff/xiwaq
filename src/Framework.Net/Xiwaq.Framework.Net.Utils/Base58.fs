// Thanks to VAVUS7 for all this
module Xiwaq.Framework.Utils.Base58

let Alphabet = @"123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz" |> Seq.toArray
let [<Literal>] AlphabetSize = 58
let EncodeBlockSizes = [| 0; 2; 3; 5; 6; 7; 9; 10; 11 |]


type EncodedBlock = char array
type DecodedBlock = byte array


module Encode =

    let letterOf i = Alphabet.[i]
    let zeroLetter = letterOf 0

    let blockToUint64 (bs: byte array) =
        Array.fold (fun n b -> (n <<< 8) ||| (uint64 b)) 0UL bs

    let encodeBlock (bs: byte array) =
        let encodedBlockSize = EncodeBlockSizes |> Array.item (Array.length bs)
        let buf = Array.create encodedBlockSize zeroLetter

        let buf, _ =
            let folder (buf, i) x =
                Array.set buf (buf.Length - 1 - i) (x % (uint64 Alphabet.Length) |> int |> letterOf)
                buf, i+1
            (folder, (buf, 0)) ||> Seq.fold
            <| let rec loop (x: uint64) = seq {
                   if x > 0UL then
                       yield x
                       yield! loop (x / uint64 AlphabetSize) }
               in loop (blockToUint64 bs)

        Seq.ofArray buf


type DecodeError =
    | Overflow
    | InvalidBlockSize
    | InvalidSymbol

exception DecodeException of DecodeError


module Decode =

    open Microsoft.FSharp.Core.Operators

    let indexOf i = Seq.tryFindIndex ((=) i) Alphabet
    let decodedBlockSize encodedBlockLength =
        Array.tryFindIndex ((=) encodedBlockLength) EncodeBlockSizes

    let decodeBlock (eb: EncodedBlock) = result {
        let blockLength = Array.length eb
        let! decodedBlockLength = decodedBlockSize blockLength |> Result.ofOptionOr DecodeError.InvalidBlockSize
        let! res =
            Seq.init blockLength (fun i -> indexOf eb.[blockLength - 1 - i], pown (uint64 AlphabetSize) i) // orders
            |> Seq.fold (fun res (ch, order) ->
                match res with
                | Ok res ->
                    match ch with
                    | Some digit ->
                        // Проверяем переполнение для случая 11 символного блока (соответствует 8-ми байтам)
                        Result.tryWith
                        <| fun () -> Checked.(+) res (Checked.(*) (uint64 digit) order)
                        |> Result.mapError (cnst DecodeError.Overflow)
                    | None -> Error DecodeError.InvalidSymbol
                // Маленький костыль, чтобы использовать функцию Seq.fold с возможностью пробросить ошибку
                | e -> e
            ) (Ok 0UL)

        // Проверка переполнения во всех остальных случаях
//        let! res =
//            if decodedBlockLength < 8 &&
//                (let max = 1UL <<< (decodedBlockLength * 8)
//                 in res < max)
//            then Ok res
//            else Error DecodeError.Overflow
        let! res =
            match Ok res with
            | Ok res when decodedBlockLength < 8 ->
                let max = 1UL <<< (decodedBlockLength * 8)
                if res < max then Ok res
                else Error DecodeError.Overflow
            | e -> e

        // Преобразование числа в блок байт
        let! res =
            let buf = Array.create decodedBlockLength 0uy
            for i in 0..decodedBlockLength-1 do
                // Можно конечно BitConverter.. Но там еще длину массивов выравнивать
                let s = byte (res >>> (i * 8))
                buf.[decodedBlockLength - 1 - i] <- s
            Ok buf

        return res
    }


open Encode
open Decode

let encode (bs: byte seq) : char seq =
    let blocks = Seq.chunkBySize 8 bs
    Seq.collect encodeBlock blocks

/// <exception cref="Xiwaq.Server.Utils.Base58.Base58DecodeException">Throws when the sequence contains an invalid char set.</exception>
let decode (cs: char seq) : byte seq =
    let blocks = Seq.chunkBySize 11 cs
    blocks
    |> Seq.collect (decodeBlock >> Result.getOkOrWithExn DecodeException) //(decodeBlock >> function Ok r -> r | Error e -> raise (Base58DecodeException e))

let decodeFold cs onNext onError onComplete init =
    let mutable s = init
    let eblocks = Seq.chunkBySize 11 cs
    let enumerator = eblocks.GetEnumerator()
    let mutable errored = None
    while enumerator.MoveNext() && errored.IsNone do
        let eblock = enumerator.Current
        match decodeBlock eblock with
        | Ok dblock ->
            for b in dblock do
                s <- onNext s b
        | Error error ->
            errored <- Some error
    enumerator.Dispose()
    match errored with
    | None -> onComplete s
    | Some error -> onError s error

module Xiwaq.Framework.Utils.Logging

open Microsoft.Extensions.Logging

open Reader


type IHasLogger =
    abstract Logger: ILogger

type IHasLoggerFactory = abstract LoggerFactory: ILoggerFactory


module Logger =

    let create category = Reader.get<#IHasLoggerFactory> <!> fun env -> env.LoggerFactory.CreateLogger(category)


module Log =

    let level lvl fmt =
        let cont s = reader {
            let! loggerProvider = Reader.get<#IHasLogger>
            let logger = loggerProvider.Logger
            do logger.Log(lvl, s)
        }
        Printf.kprintf cont fmt

    let trace fmt = level LogLevel.Trace fmt
    let debug fmt = level LogLevel.Debug fmt
    let info  fmt = level LogLevel.Information fmt
    let warn  fmt = level LogLevel.Warning fmt
    let error fmt = level LogLevel.Error fmt
    let critical fmt = level LogLevel.Critical fmt


    module Async =

        open ReaderAsync

        let level lvl fmt =
            let cont s = readerAsync {
                let! loggerProvider = Reader.get<#IHasLogger>
                let logger = loggerProvider.Logger
                do logger.Log(lvl, s)
            }
            Printf.kprintf cont fmt

        let trace fmt = level LogLevel.Trace fmt
        let debug fmt = level LogLevel.Debug fmt
        let info  fmt = level LogLevel.Information fmt
        let warn  fmt = level LogLevel.Warning fmt
        let error fmt = level LogLevel.Error fmt
        let critical fmt = level LogLevel.Critical fmt

module Xiwaq.Framework.PluginManagement.Api.DtoMappers

open Xiwaq.Framework.UserManagement.Unwrappers
open Xiwaq.Framework.PluginManagement
open Xiwaq.Framework.PluginManagement.Api


module PluginInfo =
    let toDto (info: PluginInfo) : PluginInfoDto =
        { Id = info.Id |> fun (PluginId x) -> x
          Name = info.Name |> fun (PluginName x) -> x
          AuthorId = info.AuthorId |> fun (UserId x) -> x }

[<AutoOpen>]
module Wrapping

open System.Runtime.CompilerServices


// Unwrap

let inline (|Unwrap|) (x: ^a) : 'b =
    (^a: (static member Unwrap: ^a -> 'b) x)

let inline unwrap (Unwrap x) = x

[<Extension>]
type UnwrapExtensions =
    [<Extension>]
    static member inline get_Unwrap(this) = unwrap this


// Wrap

let inline (|Wrap|) (x: 'a) : ^b =
    (^b: (static member Wrap: 'a -> ^b) x)

let inline wrap (Wrap x) = x

//[<Extension>]
//type WrapExtensions =
//    [<Extension>]
//    static member inline get_Wrap(this) = wrap this


// Rewrap

let inline (|Rewrap|) x = ((|Unwrap|) >> (|Wrap|)) x

let inline rewrap (Rewrap x) = x


[<AutoOpen>]
module Exceptions

open System

let notImplemented str = NotImplementedException(str) |> raise
let notImplementedf fmt =
    Printf.kprintf (NotImplementedException >> raise) fmt


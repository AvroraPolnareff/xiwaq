module State

type State<'s, 'a> = State of ('s -> 's * 'a)

module State =

    let run (State s) x = s x

    let retn x = State (fun s -> s, x)

    let bind f state : State<'s, 'b> =
        State ^fun s ->
            let s', x = run state s
            run (f x) s'

    let get () = State ^fun s -> s, s

type StateBuilder() =
    member _.Return(x) = State.retn x
    member _.ReturnFrom(x) = x
    member _.Zero() = State.retn ()
    member _.Bind(x, f) = State.bind f x
    member _.Combine(x, y) = State.bind (fun _ -> y) x
    member _.Delay(f) = f ()

let state = StateBuilder()

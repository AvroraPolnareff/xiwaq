[<AutoOpen>]
module SeqExtensions

[<RequireQualifiedAccess>]
module Seq =

    let replace predicate into xs = xs |> Seq.map (fun x -> if predicate x then into else x)

    let replaceOne predicate into xs =
        seq {
            let mutable replaced = false
            for x in xs do
                if predicate x && not replaced
                then yield into
                else yield x
        }

    let replacef predicate f xs = xs |> Seq.map (fun x -> if predicate x then f x else x)

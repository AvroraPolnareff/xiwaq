[<AutoOpen>]
module AsyncResult

type AsyncResult<'a, 'e> = Async<Result<'a, 'e>>

[<RequireQualifiedAccess>]
module AsyncResult =

    let retn x = x |> Ok |> Async.retn

    let bind f = Async.bind (function Ok x -> f x | Error e -> e |> Error |> Async.retn)

    let map f = bind (f >> retn)

    let ofResult x : AsyncResult<_, _> = Async.retn x

    let mapError f x = Async.map (Result.mapError f) x

type AsyncResultBuilder() =
    // AsyncResult
    member _.Bind(x, f) = AsyncResult.bind f x
    member _.Return(x) = AsyncResult.retn x
    member _.ReturnFrom(x: AsyncResult<'a, 'e>) = x
    // Async
    member _.Bind(x, f) = Async.bind f x
    // Result
    member _.Bind(x, f) = AsyncResult.bind f (Async.retn x)
    member _.ReturnFrom(x) = AsyncResult.ofResult x

let asyncResult = AsyncResultBuilder()

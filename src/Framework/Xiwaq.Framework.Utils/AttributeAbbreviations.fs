[<AutoOpen>]
module AttributeAbbreviations

type AO = AutoOpenAttribute
type RQA = RequireQualifiedAccessAttribute

namespace global

type Void private () = class end

module Void =
    let absurd (_: Void) = failwith "No sense"

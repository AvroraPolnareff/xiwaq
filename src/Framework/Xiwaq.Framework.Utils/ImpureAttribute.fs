namespace global

open System

[<AttributeUsage(AttributeTargets.Method ||| AttributeTargets.Class)>]
type ImpureAttribute() =
    inherit Attribute()

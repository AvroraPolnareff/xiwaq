[<AutoOpen>]
module Xiwaq.Framework.UserManagement.Types

type Id = System.Guid

type InputToken = InputToken of string
type Token = internal Token of string

type UserId = internal UserId of Id
type UserName = internal UserName of string
type UserTag = internal UserTag of uint16
type UserFullName = internal UserFullName of UserName * UserTag
type UserPassword = internal UserPassword of string
type UserEmail = internal UserEmail of string

type User =
    { Id: UserId
      FullName: UserFullName }

type Me =
    { User: User
      Token: Token }


type GetMeError =
    | InvalidToken
    | Other of string


type SignUpData =
    { Name: UserName
      Email: UserEmail
      Password: UserPassword }

type SignUpError =
    | EmailIsAlreadyTaken
    | Other of string


type SignInData =
    { Email: UserEmail
      Password: UserPassword }

type SignInError =
    | InvalidData
    | Other of string

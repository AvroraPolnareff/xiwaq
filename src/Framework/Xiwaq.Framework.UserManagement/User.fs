namespace Xiwaq.Framework.UserManagement

open System
open System.Globalization


[<RequireQualifiedAccess>]
module UserName =

    let UnresolvedSymbols = "\t\r\n " :> _ seq

    type ParseError =
        | TooLong
        | TooShort
        | UnresolvedSymbol

    let parse (str: string) = result {
        return!
            if str.Length < 3 then
                Error ParseError.TooShort
            elif str.Length > 25 then
                Error ParseError.TooLong
            elif String.exists (fun c -> Seq.contains c UnresolvedSymbols) str then
                Error ParseError.UnresolvedSymbol
            else
                Ok ^ UserName str
    }


module UserTag =

    type ParseError =
        | ParseError

    let parse (str: string) =
        UInt16.TryParse(str, NumberStyles.HexNumber, CultureInfo.InvariantCulture)
        |> function
        | true, x -> Ok ^ UserTag x
        | false, _ -> Error ParseError.ParseError


module UserFullName =

    type ParseError =
        | InvalidSyntax
        | NameParseError of UserName.ParseError
        | TagParseError of UserTag.ParseError

    let parse (str: string) =
        result {
            let! nameStr, tagStr =
                match str with
                | String.Split '#' [ nameStr; tagStr ] -> Ok (nameStr, tagStr)
                | _ -> Error ParseError.InvalidSyntax
            let! name = UserName.parse nameStr |> Result.mapError ParseError.NameParseError
            let! tag = UserTag.parse tagStr |> Result.mapError ParseError.TagParseError
            return UserFullName (name, tag)
        }

    let display (UserFullName (UserName name, UserTag tag)) = sprintf "%s#%4X" name tag


module UserPassword =

    type ParseError =
        | TooShort
        | TooLong

    let parse (str: string) = result {
        let len = String.length str
        match len with
        | _ when len < 5 ->
            return! Error ParseError.TooShort
        | _ when len > 255 ->
            return! Error ParseError.TooLong
        | _ ->
            return UserPassword str
    }


module UserEmail =

    module Unchecked = let create = UserEmail

    open System.Text.RegularExpressions

    let private EmailRegex = Regex(@"^([\w._-]+)@((\w+)\.(?:[^.]+))$")

    type ParseError =
        | InvalidEmailSyntax

    let parse str = result {
        let m = EmailRegex.Match(str)
        if m.Success
        then return UserEmail str
        else return! Error ParseError.InvalidEmailSyntax
    }

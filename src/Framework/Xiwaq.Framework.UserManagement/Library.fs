﻿namespace Xiwaq.Framework.UserManagement

type IAuthenticationService =
    abstract SignUp: SignUpData -> AsyncResult<unit, SignUpError>
    abstract SignIn: SignInData -> AsyncResult<Me, SignInError>
    abstract GetMe: InputToken -> AsyncResult<Me, GetMeError>

type IUserManager =
    abstract GetUserById: UserId -> Async<User option>
    abstract GetUserByFullName: UserFullName -> Async<User option>
    abstract FindUsersByName: UserName -> Async<User seq>

type ITokenProvider =
    abstract Token: Token

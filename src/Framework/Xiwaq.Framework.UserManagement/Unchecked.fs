namespace Xiwaq.Framework.UserManagement

(*
Token
UserId
UserName
UserTag
UserFullName
UserPassword
UserEmail
*)
// ^(\w+)$


//     [<RQA>] module $1 = module Unchecked = let create = $1
module Wrappers =
    [<RQA>] module Token = module Unchecked = let create = Token
    [<RQA>] module UserId = module Unchecked = let create = UserId
    [<RQA>] module UserName = module Unchecked = let create = UserName
    [<RQA>] module UserTag = module Unchecked = let create = UserTag
    [<RQA>] module UserFullName = module Unchecked = let create = UserFullName
    [<RQA>] module UserPassword = module Unchecked = let create = UserPassword
    [<RQA>] module UserEmail = module Unchecked = let create = UserEmail

//     let (|$1|) ($1 x) = x
module Unwrappers =
    let (|Token|) (Token x) = x
    let (|UserId|) (UserId x) = x
    let (|UserName|) (UserName x) = x
    let (|UserTag|) (UserTag x) = x
    let (|UserFullName|) (UserFullName (x1, x2)) = x1, x2
    let (|UserPassword|) (UserPassword x) = x
    let (|UserEmail|) (UserEmail x) = x

﻿namespace Xiwaq.Framework.PluginManagement


type IPluginManager =
    abstract GetAllPluginInfos: unit -> Async<PluginInfo seq>
    abstract GetInfoById: PluginId -> Async<PluginInfo>

type IPluginScriptLinkProvider =
    abstract GetLink: PluginId -> string

[<AutoOpen>]
module Xiwaq.Framework.PluginManagement.Types

open Xiwaq.Framework.UserManagement


type Id = System.Guid

type PluginId = PluginId of Id

type PluginName = PluginName of string

type PluginInfo =
    { Id: PluginId
      Name: PluginName
      AuthorId: UserId }

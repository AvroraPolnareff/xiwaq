﻿namespace Xiwaq.Framework.Utils

open Elmish
open Fable.Core


module Elmish =

    let invalidState currentState : 'state * Cmd<'msg> =
        JS.console.warn("Invalid state has reached")
        currentState, Cmd.none

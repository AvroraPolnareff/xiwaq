﻿namespace Xiwaq.Framework.UserManagement.Client.Fable

open System
open Fable.Core
open Thoth.Fetch

open Xiwaq.Framework.UserManagement
open Xiwaq.Framework.UserManagement.Api


exception FetchException of FetchError

module FetchAuthenticationService =

    open Xiwaq.Framework.UserManagement.Api.Mappers.AuthenticationService

    let signUp host (data: SignUpData) : AsyncResult<unit, SignUpError> =
        promise {
            let uri = Uri(host, SignUp.route)
            let dto = SignUpData.toDto data
            let! resp = Fetch.tryPost(string uri, dto)
            match resp with
            | Ok resp -> return resp
            | Error err -> return raise (FetchException err)
        } |> Async.AwaitPromise

    let signIn host (data: SignInData) : AsyncResult<Me, SignInError> =
        promise {
            let uri = Uri(host, SignIn.route)
            let dto = data |> SignInData.toDto
            let! resp = Fetch.tryPost(string uri, dto)
            match resp with
            | Ok (resp: SignIn.Response) ->
                let r =
                    match resp.Success with
                    | true -> Ok (notImplemented "")
                    | _ -> Error (notImplemented "")
                return r
            | Error err -> return raise (FetchException err)
        } |> Async.AwaitPromise

    let getMe host (inputToken: InputToken) : AsyncResult<Me, GetMeError> =
        promise {
            let uri = Uri(host, GetMe.route)
            let req = { GetMe.Request.InputToken = inputToken |> fun (InputToken x) -> x }
            let! resp = Fetch.tryPost(string uri, req)
            match resp with
            | Ok resp -> return resp
            | Error err -> return raise (FetchException err)
        } |> Async.AwaitPromise


type FetchAuthenticationService(host) =
    interface IAuthenticationService with
        member _.SignUp(data) = FetchAuthenticationService.signUp host data
        member _.SignIn(data) = FetchAuthenticationService.signIn host data
        member _.GetMe(inputToken) = FetchAuthenticationService.getMe host inputToken

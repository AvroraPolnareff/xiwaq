﻿namespace Xiwaq.Framework.PluginManagement

open Elmish
open Reader
open FSharp.Control
open Xiwaq.Framework.PluginManagement


type IController =
    abstract Listen<'PEvent> : thisPluginInfo: PluginInfo * pName: string -> IAsyncObservable<'PEvent>

type ControllerContext =
    { Controller: IController
      PluginInfo: PluginInfo }

type AsyncRxC<'a> = Reader<ControllerContext, IAsyncObservable<'a>>


type DynamicState = DynamicState of obj
type DynamicEvent = DynamicEvent of obj
type DynamicCommand = DynamicCommand of obj

type PluginDynamicEvent = PluginInfo * DynamicEvent
type PluginDynamicCommand = PluginInfo * DynamicCommand
type PluginDynamicState = PluginInfo * DynamicState



type PluginProgram<'state, 'command, 'event> =
    { init: PluginInfo -> 'state
      update: 'event -> 'state -> 'state
      view: 'state -> ('command -> unit) -> Fable.React.ReactElement
      stream: 'state -> IAsyncObservable<'command> -> AsyncRxC<'event> }

type DynamicPluginProgram = PluginProgram<DynamicState, DynamicCommand, DynamicEvent>

type Plugin =
    { Info: PluginInfo
      Program: DynamicPluginProgram }


type IPluginLoader =
    abstract Load: PluginId -> Async<Plugin>


module Internal =

    module GlobalPluginProgramLoadedCallback =

        open Fable.Core

        [<Emit "globalThis.XiwaqGlobalPluginProgramLoadedCallback">]
        let mutable private callback: (DynamicPluginProgram -> unit) option = jsNative

        let push dynamicProgram =
            match callback with
            | Some callback -> callback dynamicProgram
            | None -> invalidOp "Plugin program cannot be loaded until the callback is set."

        /// In callback `loader` must be called `push` by someone
        let usingAsync (loader: unit -> Async<unit>) : Async<DynamicPluginProgram> = async {
            let mutable program = None
            if callback.IsSome then
                JS.console.warn(
                    "Global-plugin-program-loaded-callback has been overwritten. " +
                    "This might be due to asynchronous plugin loading, and loading errors can occur because of this.")
            callback <- Some ^fun x -> program <- Some x
            do! loader ()
            callback <- None
            match program with
            | None -> return invalidOp "push wasn't called in loader"
            | Some program -> return program
        }

open Internal



[<RequireQualifiedAccess>]
module PluginProgram =

    open Fable.Core.JsInterop

    let mkPlugin init update view stream =
        let program: PluginProgram<'state, 'command, 'event> = { init = init; update = update; view = view; stream = stream }
        program

    let run (program: PluginProgram<_, _, _>) =
        let dynamicProgram: DynamicPluginProgram = !!program
        GlobalPluginProgramLoadedCallback.push dynamicProgram

module Xiwaq.Framework.PluginManagement.MsgStream

open Reader
open FSharp.Control


module AsyncRxC =

    let map (f: 'a -> 'b) (x: AsyncRxC<'a>) : AsyncRxC<'b> =
        x |> Reader.map ^fun xs -> AsyncRx.map f xs




type AsyncRxCBuilder() =

    member _.Bind(x: IAsyncObservable<'a>, f: 'a -> AsyncRxC<'b>): AsyncRxC<'b> =
        reader {
            let! controller = Reader.get
            let f = f >> fun x -> Reader.run x controller
            return asyncRx.Bind(x, f)
        }
    member _.Bind(x: AsyncRxC<'a>, f: 'a -> AsyncRxC<'b>): AsyncRxC<'b> =
        reader {
            let! x' = x
            let! controller = Reader.get
            return asyncRx {
                let! x'' = x'
                yield! f x'' |> Reader.run <| controller
            }
        }

    member _.Yield(x): AsyncRxC<'a> = Reader.retn ^ asyncRx.Yield(x)

    member _.Zero(): AsyncRxC<'a> = Reader.retn ^ asyncRx.Zero()

    member _.Delay(f): AsyncRxC<'a> =
        reader {
            let! controller = Reader.get
            let f = f >> fun x -> Reader.run x controller
            return asyncRx.Delay(f)
        }

    member _.Combine(x, y): AsyncRxC<'a> = reader {
        let! (x: IAsyncObservable<'a>) = x
        let! (y: IAsyncObservable<'a>) = y
        return asyncRx.Combine(x, y)
    }

    member _.YieldFrom(x: AsyncRxC<_>): AsyncRxC<'a> = x |> Reader.map asyncRx.YieldFrom
    member _.YieldFrom(x: Async<_>): AsyncRxC<'a> = x |> asyncRx.YieldFrom |> Reader.retn
    member _.YieldFrom(x: IAsyncObservable<_>): AsyncRxC<'a> = x |> asyncRx.YieldFrom |> Reader.retn

let asyncRxC = AsyncRxCBuilder()


[<AutoOpen>]
module Helpers =
    module AsyncRxC =

        let mergeInner (x: AsyncRxC<AsyncRxC<'a>>) : AsyncRxC<'a> =
            reader {
                let! c = Reader.get
                let x' = x |> Reader.run <| c
                let x'' = x' |> AsyncRx.map ^fun x -> Reader.run x c
                return asyncRx {
                    yield! x'' |> AsyncRx.mergeInner
                }
            }


let listen<'PEvent> (pName: string) : AsyncRxC<_> =
    reader {
        let! ctx = Reader.get
        return asyncRx {
            yield! ctx.Controller.Listen<'PEvent>(ctx.PluginInfo, pName)
        }
    }

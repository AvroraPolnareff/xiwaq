namespace Xiwaq.Framework.PluginManagement.Client

open Xiwaq.Framework.PluginManagement
open Xiwaq.Framework.PluginManagement.Api


module PluginScriptLinkProvider =

    open System

    let getLink host (PluginId pid) =
        let path = PluginScriptLinkProvider.GetLink.route.Replace("{id}", string pid)
        let uri = Uri(host, path)
        uri |> string


type PluginScriptLinkProvider(host) =
    interface IPluginScriptLinkProvider with
        member _.GetLink(id) = PluginScriptLinkProvider.getLink host id

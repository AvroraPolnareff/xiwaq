module Xiwaq.Plugins.Squared.Library

open Reader
open Xiwaq
open Xiwaq.Framework.PluginManagement.Types
open Xiwaq.Framework.PluginManagement.MsgStream


type Command =
    | SwapColor

type Event =
    | Incremented
    | Decremented
    | Reset
    | ColorSwapped

type ColorState = Red | Black

type State =
    { PluginInfo: PluginInfo
      JustCount: int
      Color: ColorState }

module SquaredPlugin =

    open FSharp.Control
    open Xiwaq.Framework.PluginManagement

    let init info =
        { PluginInfo = info
          JustCount = 0
          Color = Black }

    let computeSquared inc c =
        c |> (float >> sqrt >> int) |> fun x -> inc x 1 |> fun c -> c * c

    let update msg state =
        match msg with
        | Event.ColorSwapped ->
            match state.Color with
            | Red -> { state with Color = Black }
            | Black -> { state with Color = Red }
        | Event.Reset -> { state with JustCount = 0 }
        | Event.Incremented ->
            { state with JustCount = computeSquared (+) state.JustCount }
        | Event.Decremented ->
            { state with JustCount = computeSquared (-) state.JustCount }

    module R = Fable.React.Standard
    module R = Fable.React.Helpers
    module R = Fable.React.Props
    let view state dispatch =
        R.fragment [ ] [
            R.span [
                match state.Color with
                | Red -> R.Style [ R.Color "red" ]
                | Black -> R.Style [ R.Color "black" ]
            ] [
                R.str ^ sprintf "Squared count: %i" (pown state.JustCount 2)
            ]
            R.button [
                R.OnClick (fun _ -> dispatch SwapColor)
            ] [
                R.str "Swap color!"
            ]
        ]

    let stream (state: State) (commands: IAsyncObservable<Command>) =
        asyncRxC {
            yield asyncRxC {
                match! commands with
                | SwapColor -> yield Event.ColorSwapped
            }
            yield asyncRxC {
                let! baseEvent = listen<Base.Event> "Base"
                match baseEvent with
                | Base.Event.Incremented -> yield Event.Incremented
                | Base.Event.Decremented -> yield Event.Decremented
                | Base.Event.Reset -> yield Event.Reset
                | _ -> ()
            }
        }
        |> AsyncRxC.mergeInner


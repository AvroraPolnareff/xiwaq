module Xiwaq.Plugins.Squared.Program

open Xiwaq.Framework.PluginManagement
open Xiwaq.Plugins.Squared.Library


let init = SquaredPlugin.init
let update = SquaredPlugin.update
let view = SquaredPlugin.view
let stream = SquaredPlugin.stream

PluginProgram.mkPlugin init update view stream
|> PluginProgram.run

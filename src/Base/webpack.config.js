const path = require('path');
const webpack = require('webpack');


function resolve(filePath) {
    return path.isAbsolute(filePath) ? filePath : path.join(__dirname, filePath);
}


const CONFIG = {
    basePlugin: "./Xiwaq.Base/Xiwaq.Base.fsproj",
    squaredPlugin: "./Xiwaq.Plugins.Squared/Xiwaq.Plugins.Squared.fsproj",
    outputDir: "./deploy",
    babel: {
        presets: [
            ['@babel/preset-env', {
                modules: false,
                // This adds polyfills when needed. Requires core-js dependency.
                // See https://babeljs.io/docs/en/babel-preset-env#usebuiltins
                // Note that you still need to add custom polyfills if necessary (e.g. whatwg-fetch)
                useBuiltIns: 'usage',
                corejs: 3
            }]
        ],
    }
}


module.exports = (env, argv) => {
    return {
        entry: {
            "485fbe49-f26f-4236-8808-16d463268e10": [
                resolve(CONFIG.basePlugin),
            ],
            "f301a6a7-be04-4140-bc21-4c9970998b68": [
                resolve(CONFIG.squaredPlugin)
            ]
        },
        output: {
            path: resolve(CONFIG.outputDir),
            filename: "[name].js"
        },
        resolve: {
            modules: [
                resolve("./node_modules"),
                // resolve("./Xiwaq.Base.Abstractions"),
                // resolve("../Framework"),
                // resolve("../Framework.Fable"),
            ]
        },
        module: {
            rules: [
                {
                    test: /\.fs(x|proj)?$/,
                    use: {
                        loader: 'fable-loader',
                        options: {
                            babel: CONFIG.babel
                        }
                    }
                },
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: {
                        loader: 'babel-loader',
                        options: CONFIG.babel
                    },
                }
            ]
        }
    }
}

module Xiwaq.Base.Program

open Xiwaq.Framework.PluginManagement


let init = Base.init
let update = Base.update
let view = Base.view
let stream = Base.stream

PluginProgram.mkPlugin init update view stream
|> PluginProgram.run

﻿namespace Xiwaq.Base

open Elmish

open Xiwaq.Framework.PluginManagement
open Xiwaq.Base
open Xiwaq.Framework.PluginManagement.MsgStream

module R = Fable.React.Standard
module R = Fable.React.Helpers
module R = Fable.React.Props


module Base =

    open FSharp.Control

    let init (info: PluginInfo) =
        { PluginInfo = info
          Count = 0 }

    let update event state =
        match event with
        | Event.Incremented -> { state with Count = state.Count + 1 }
        | Event.Decremented -> { state with Count = state.Count - 1 }
        | Event.Reset -> { state with Count = 0 }

    let view state dispatch =
        let button name msg =
            R.button [ R.OnClick (fun _ -> dispatch msg) ] [
                R.str name
            ]

        R.div [] [
            R.span [] [
                R.str (sprintf "Count: %i" state.Count)
            ]
            button "Increment" Command.Increment
            button "Decrement" Command.Decrement
            button "Reset" Command.Reset
            button "Double Increment" Command.DoubleIncrement
        ]


    let stream (state: State) (commands: IAsyncObservable<_>) =
        asyncRxC {
            let! command = commands
            match command with
            | Command.Increment -> Event.Incremented
            | Command.Decrement -> Event.Decremented
            | Command.Reset -> Event.Reset
            | Command.DoubleIncrement -> yield! AsyncRx.ofSeq [ Event.Incremented; Event.Incremented ]
        }

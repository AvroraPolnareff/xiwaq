﻿namespace Xiwaq.Base

open Xiwaq.Framework.PluginManagement


type Command =
    | Increment
    | DoubleIncrement
    | Decrement
    | Reset

type Event =
    | Incremented
    | Decremented
    | Reset

type State =
    { PluginInfo: PluginInfo
      Count: int }

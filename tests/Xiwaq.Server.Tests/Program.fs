﻿module Xiwaq.Server.Tests.Program

open Expecto

[<EntryPoint>]
let main argv =
    Tests.runTestsInAssemblyWithCLIArgs [ Parallel ] argv

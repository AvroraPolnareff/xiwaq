module Xiwaq.Server.Tests.Base58

open System
open Expecto
open Expecto
open Xiwaq.Server.Utils

open FsCheck
//type BytesGen() =
//    static member Bytes() : Arbitrary<byte seq> =
//        let genByte = byte <!> Gen.choose (0x00, 0xFF)
//        let genBytes = Gen.
//        ()

let bytesGen =
    Gen.sized <| fun size -> gen {
        let genByte = byte <!> Gen.choose (0x00, 0xFF)
        return! Seq.replicate size genByte |> Gen.sequence |> Gen.map Seq.ofList
    }

type Base58CharSeq = Base58CharSeq of char seq

let base58CharSeqGen = gen {
    let! bytes = bytesGen
    return Base58.encode bytes |> Base58CharSeq
}

type BytesGen() =
    static member Bytes() = Arb.fromGen bytesGen

type Base58CharSeqGen() =
    static member Base58CharSeq() = Arb.fromGen base58CharSeqGen

let fsCheckConfig =
    { FsCheckConfig.defaultConfig with
        arbitrary = [ typeof<BytesGen>; typeof<Base58CharSeqGen> ] }

let charsAsStr (cs: char seq) = String.Concat cs

let bytesAsHexStr (bs: byte seq) =
    String.Join(" ", Seq.map (sprintf "%X") bs)

let encodeData =
    [
        [ 0x00 ], "11"
        [ 0x39 ], "1z"
        [ 0xFF ], "5Q"
        [ 0x00; 0x00 ], "111"
        [ 0x00; 0x39 ], "11z"
        [ 0x01; 0x00 ], "15R"
        [ 0xFF; 0xFF ], "LUv"
        [ 0x00; 0x00; 0x00 ], "11111"
        [ 0x00; 0x00; 0x39 ], "1111z"
        [ 0x01; 0x00; 0x00 ], "11LUw"
        [ 0xFF; 0xFF; 0xFF ], "2UzHL"
        [ 0x00; 0x00; 0x00; 0x39 ], "11111z"
        [ 0xFF; 0xFF; 0xFF; 0xFF ], "7YXq9G"
        [ 0x00; 0x00; 0x00; 0x00; 0x39 ], "111111z"
        [ 0xFF; 0xFF; 0xFF; 0xFF; 0xFF ], "VtB5VXc"
        [ 0x00; 0x00; 0x00; 0x00; 0x00; 0x39 ], "11111111z"
        [ 0xFF; 0xFF; 0xFF; 0xFF; 0xFF; 0xFF ], "3CUsUpv9t"
        [ 0x00; 0x00; 0x00; 0x00; 0x00; 0x00; 0x39 ], "111111111z"
        [ 0xFF; 0xFF; 0xFF; 0xFF; 0xFF; 0xFF; 0xFF ], "Ahg1opVcGW"
        [ 0x00; 0x00; 0x00; 0x00; 0x00; 0x00; 0x00; 0x39 ], "1111111111z"
        [ 0xFF; 0xFF; 0xFF; 0xFF; 0xFF; 0xFF; 0xFF; 0xFF ], "jpXCZedGfVQ"
        [ 0x00; 0x00; 0x00; 0x00; 0x00; 0x00; 0x00; 0x00 ], "11111111111"
        [ 0x00; 0x00; 0x00; 0x00; 0x00; 0x00; 0x00; 0x01 ], "11111111112"
        [ 0x00; 0x00; 0x00; 0x00; 0x00; 0x00; 0x00; 0x08 ], "11111111119"
        [ 0x00; 0x00; 0x00; 0x00; 0x00; 0x00; 0x00; 0x09 ], "1111111111A"
        [ 0x00; 0x00; 0x00; 0x00; 0x00; 0x00; 0x00; 0x3A ], "11111111121"
        [ 0x00; 0xFF; 0xFF; 0xFF; 0xFF; 0xFF; 0xFF; 0xFF ], "1Ahg1opVcGW"
        [ 0x06; 0x15; 0x60; 0x13; 0x76; 0x28; 0x79; 0xF7 ], "22222222222"
        [ 0x05; 0xE0; 0x22; 0xBA; 0x37; 0x4B; 0x2A; 0x00 ], "1z111111111"
        [ 0x05; 0xE0; 0x22; 0xBA; 0x37; 0x4B; 0x2A; 0x00; 0x00 ], "1z11111111111"
    ] |> Seq.map (fun (bs, cs) -> Seq.map byte bs, cs)

let decodeData =
    [
        // 1-byte block
        "1", Error Base58.Decode.DecodeError.InvalidBlockSize
        "z", Error Base58.Decode.DecodeError.InvalidBlockSize
        // 2-byte block
        "11", Ok [ 0x00 ]
        "5Q", Ok [ 0xFF ]
        "5R", Error Base58.Decode.DecodeError.Overflow;
        "zz", Error Base58.Decode.DecodeError.Overflow;
        // 3-bytes block
        "111", Ok [ 0x00; 0x00 ]
        "LUv", Ok [ 0xFF; 0xFF ]
        "LUw", Error Base58.Decode.DecodeError.Overflow;
        "zzz", Error Base58.Decode.DecodeError.Overflow;
        // 4-bytes block
        "1111", Error Base58.Decode.DecodeError.InvalidBlockSize
        "zzzz", Error Base58.Decode.DecodeError.InvalidBlockSize
        // 5-bytes block
        "11111", Ok [ 0x00; 0x00; 0x00 ]
        "2UzHL", Ok [ 0xFF; 0xFF; 0xFF ]
        "2UzHM", Error Base58.Decode.DecodeError.Overflow;
        "zzzzz", Error Base58.Decode.DecodeError.Overflow;
        // 6-bytes block
        "111111", Ok [ 0x00; 0x00; 0x00; 0x00 ]
        "7YXq9G", Ok [ 0xFF; 0xFF; 0xFF; 0xFF ]
        "7YXq9H", Error Base58.Decode.DecodeError.Overflow;
        "zzzzzz", Error Base58.Decode.DecodeError.Overflow;
        // 7-bytes block
        "1111111", Ok [ 0x00; 0x00; 0x00; 0x00; 0x00 ]
        "VtB5VXc", Ok [ 0xFF; 0xFF; 0xFF; 0xFF; 0xFF ]
        "VtB5VXd", Error Base58.Decode.DecodeError.Overflow;
        "zzzzzzz", Error Base58.Decode.DecodeError.Overflow;
        // 8-bytes block
        "11111111", Error Base58.Decode.DecodeError.InvalidBlockSize
        "zzzzzzzz", Error Base58.Decode.DecodeError.InvalidBlockSize
        // 9-bytes block
        "111111111", Ok [ 0x00; 0x00; 0x00; 0x00; 0x00; 0x00 ]
        "3CUsUpv9t", Ok [ 0xFF; 0xFF; 0xFF; 0xFF; 0xFF; 0xFF ]
        "3CUsUpv9u", Error Base58.Decode.DecodeError.Overflow;
        "zzzzzzzzz", Error Base58.Decode.DecodeError.Overflow;
        // 10-bytes block
        "1111111111", Ok [ 0x00; 0x00; 0x00; 0x00; 0x00; 0x00; 0x00 ]
        "Ahg1opVcGW", Ok [ 0xFF; 0xFF; 0xFF; 0xFF; 0xFF; 0xFF; 0xFF ]
        "Ahg1opVcGX", Error Base58.Decode.DecodeError.Overflow;
        "zzzzzzzzzz", Error Base58.Decode.DecodeError.Overflow;
        // 11-bytes block
        "11111111111", Ok [ 0x00; 0x00; 0x00; 0x00; 0x00; 0x00; 0x00; 0x00 ]
        "jpXCZedGfVQ", Ok [ 0xFF; 0xFF; 0xFF; 0xFF; 0xFF; 0xFF; 0xFF; 0xFF ]
        "jpXCZedGfVR", Error Base58.Decode.DecodeError.Overflow;
        "zzzzzzzzzzz", Error Base58.Decode.DecodeError.Overflow;
        // Invalid symbols
        "01111111111", Error Base58.Decode.DecodeError.InvalidSymbol;
        "11111111110", Error Base58.Decode.DecodeError.InvalidSymbol;
        "11111011111", Error Base58.Decode.DecodeError.InvalidSymbol;
        "I1111111111", Error Base58.Decode.DecodeError.InvalidSymbol;
        "O1111111111", Error Base58.Decode.DecodeError.InvalidSymbol;
        "l1111111111", Error Base58.Decode.DecodeError.InvalidSymbol;
        "_1111111111", Error Base58.Decode.DecodeError.InvalidSymbol;
    ] |> Seq.map (fun (cs, bs) -> cs, Result.map (Seq.map byte) bs)


[<Tests>]
let tests = testList "Base58" [
    test "Encode data" {
        encodeData
        |> Seq.iter (fun (actualBs, expectedCs) ->
            Expect.sequenceEqual
            <| Base58.encode actualBs
            <| expectedCs
            <| sprintf "[ %s ] = \"%s\"" (bytesAsHexStr actualBs) (charsAsStr expectedCs)
        )
    }

    test "Decode data" {
        decodeData
        |> Seq.iter (fun (actualCs, expectedResult) ->
            match expectedResult with
            | Ok expectedBs ->
                Expect.sequenceEqual
                <| Base58.decode actualCs
                <| expectedBs
                <| ""
            | Error expectedError ->
                Expect.throwsC
                    <| fun () -> Base58.decode actualCs |> Seq.iter ignore
                    <| fun ex ->
                        match ex with
                        | Base58.Decode.Base58DecodingException actualError ->
                            Expect.equal actualError expectedError ""
                        | _ -> Expect.isTrue false "Decoding should throw the error."
        )
    }

    testPropertyWithConfig fsCheckConfig "encode >> decode = id" <| fun bs ->
        let f = Base58.encode >> Base58.decode
        Expect.sequenceEqual
        <| bs
        <| f bs
        <| ""

    testPropertyWithConfig fsCheckConfig "decode >> encode = id (with valid Base58)" <| fun (Base58CharSeq cs) ->
        let f = Base58.decode >> Base58.encode
        Expect.sequenceEqual
        <| cs
        <| f cs
        <| ""

    testPropertyWithConfig fsCheckConfig "encode >> decode >> encode >> decode >> encode >> decode = id" <| fun bs ->
        let f = Base58.encode >> Base58.decode
        let f = f >> f >> f
        Expect.sequenceEqual
        <| bs
        <| f bs
        <| ""
]

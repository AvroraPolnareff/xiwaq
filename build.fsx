#r "paket: groupref Build //"
#load "./.fake/build.fsx/intellisense.fsx"
#r "netstandard"

open System
open Fake.Core
open Fake.DotNet
open Fake.IO
open Fake.DotNet.Testing


Target.initEnvironment ()


module UserManagement =

    module Server =

        module Build =

            let target parameter =
                Trace.log "Building UserManagement.Server!"
                ()



Target.create "UserManagement.Server.Build" UserManagement.Server.Build.target

Target.runOrDefault "UserManagement.Server.Build"
